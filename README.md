# recipeapp-frontend



## Overview

Recipeapp-frontend is developed as part of ABN Amro technical assessment. The frontend application performs operation such that user can view recipes,update or delete recipes and edit recipe.

## System Design

Application is developed using Angular12.Created compomnents to perform different operations.

## Prerequisites
you need to have Node. js & npm installed on your machine.

## Steps to run application
- npm install
- ng serve

## Code Covers
- User registration
- Rest calls to create, update,delete, edit recipes
- Basic authentication for rest calls


## Future scope
- Exception Handling is not covered
- login functionality is not covered
- validations are not covered


