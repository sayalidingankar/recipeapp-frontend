import { Ingredients } from "./Ingredients";

export class Recipe {
    id!: number;
    recipeName: string | undefined;
    vegIndicator: string | undefined;
    cookingInstruction: string | undefined;
    ingredients:Array<Ingredients>=new Array;
    servedForPeople: string | undefined;
   
}
