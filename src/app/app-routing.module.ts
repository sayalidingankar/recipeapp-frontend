import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateRecipeComponent } from './Components/create-recipe/create-recipe.component';
import { EditComponentComponent } from './Components/edit-component/edit-component.component';

import { ReceipesComponent } from './Components/receipes/receipes.component';

import { RegisterComponent } from './Components/register/register.component';

const routes: Routes = [
  { path: '', redirectTo: 'register' ,pathMatch:'full'},
  {path: 'register',component:RegisterComponent},
  { path: 'recipe', component: ReceipesComponent },
  {path: 'editRecipe', component: EditComponentComponent },
  {path: 'createRecipe', component: CreateRecipeComponent },
  
  { path: '**', redirectTo: 'register' ,pathMatch:'full'}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
