import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Recipe } from 'src/app/Model/Recipe';


@Component({
  selector: 'app-receipes',
  templateUrl: './receipes.component.html',
  styleUrls: ['./receipes.component.scss']
})
export class ReceipesComponent implements OnInit {
recipes:any;
username:any;
password:any;
errorMessage:string="";
  constructor(private http: HttpClient,private route: Router) {

    this.username=localStorage.getItem("user");
    this.password=localStorage.getItem("password");
   
  }

  ngOnInit(): void {
     
  
    this.getRecipes();
    
  }

  getRecipes(){
    const headers = new HttpHeaders({ Authorization: 'Basic ' + window.btoa(this.username + ':' + this.password) });
    
    this.http.get<any>("http://localhost:8080/receipes/getReceipes",{headers}).subscribe(
      response=>{
        this.recipes=response;
        if(this.recipes.length == 0){
          this.errorMessage="No recipes added.Please Add new recipe!!";
        }
         


      }
    )
  }
  deleteRecipe(id:any){
    
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(this.username + ':' + this.password) ,'Content-type': 'application/json'});
    this.http.delete("http://localhost:8080/receipes/deleteReceipe/"+id,{headers}).subscribe(
      response=>{
          location.reload();
         }
    )
  }
  editRecipe(recipe:any){
    let rec:Recipe=JSON.parse(JSON.stringify(recipe));
    
    localStorage.setItem("recipe",JSON.stringify(rec));
    this.route.navigate(["editRecipe"]);
  }
  createRecipe(){
    this.route.navigate(["createRecipe"]);
  }
}
