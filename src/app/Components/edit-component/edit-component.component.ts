import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ingredients } from 'src/app/Model/Ingredients';
import { Recipe } from 'src/app/Model/Recipe';

@Component({
  selector: 'app-edit-component',
  templateUrl: './edit-component.component.html',
  styleUrls: ['./edit-component.component.scss']
})
export class EditComponentComponent implements OnInit {
recipe:Recipe= new Recipe();
username:any;
password:any;
ingredientStr:string="";
  constructor(private http: HttpClient,private route: Router) { 
   
     this.recipe=JSON.parse((localStorage.getItem("recipe"))|| '{}');
     let ingredientsList=this.recipe.ingredients;
     let ingredientSet: Array<string> = new Array<string>();
     ingredientsList.forEach((element) => {
      ingredientSet.push(element.ingredientName);
    });
      this.ingredientStr=ingredientSet.join(",");
     this.username=localStorage.getItem("user");
     this.password=localStorage.getItem("password");
  }

  ngOnInit(): void {
  }

  editRecipe(){

    let ingredientSet: Array<Ingredients> = new Array<Ingredients>();
    var ingredientArray=this.ingredientStr.split(",");
    ingredientArray.forEach((element: string) => {
      
      ingredientSet.push(new Ingredients(element));
    });
    this.recipe.ingredients=ingredientSet;
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(this.username + ':' + this.password) ,'Content-type': 'application/json'});
    this.http.put("http://localhost:8080/receipes/updateReceipe/"+this.recipe.id,JSON.stringify(this.recipe),{headers}).subscribe(
      response=>{
           this.route.navigate(["/recipe"]);
         }
    )
  }

}
