import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Model/User';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { RecipeResponse } from 'src/app/Model/RecipeResponse';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    user:User=new User();
    errorMessage:string="";
  constructor(private http: HttpClient,private route: Router) {
    
   }

  ngOnInit(): void {
  }
  registerUser(){
    const headers={'Content-type': 'application/json'}
    this.user.role="Admin";
    this.http.post<any>("http://localhost:8080/user/register",JSON.stringify(this.user),{headers}).subscribe(
      response=>{
       
        localStorage.setItem("user",this.user.username);
        localStorage.setItem("password",this.user.password);
           this.route.navigate(["/recipe"]);
        
      },
      (error:HttpErrorResponse) => {   
       
        if(error.status == 302){
          this.errorMessage="User Already exist with given username";
        }else{
          this.errorMessage=error.statusText;
        }
        this.user.username="";
        this.user.password="";
        
              
      }
    )
  }
}
