import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ingredients } from 'src/app/Model/Ingredients';
import { Recipe } from 'src/app/Model/Recipe';

@Component({
  selector: 'app-create-recipe',
  templateUrl: './create-recipe.component.html',
  styleUrls: ['./create-recipe.component.scss']
})
export class CreateRecipeComponent implements OnInit {
  recipe:Recipe=new Recipe();
  ingredients:String="";
  username:any;
  password:any;
  constructor(private route: Router,private http: HttpClient) { 

    this.username=localStorage.getItem("user");
    this.password=localStorage.getItem("password");
  }

  ngOnInit(): void {
  }
  
  createRecipe(){
  
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(this.username + ':' + this.password) ,'Content-type': 'application/json'});
    let ingredientSet: Array<Ingredients> = new Array<Ingredients>();
    var ingredientArray=this.ingredients.split(",");
    ingredientArray.forEach((element: string) => {
      
      ingredientSet.push(new Ingredients(element));
    });
     this.recipe.ingredients=ingredientSet
    
    this.http.post("http://localhost:8080/receipes/createReceipe",JSON.stringify(this.recipe),{headers}).subscribe(
      response=>{
           this.route.navigate(["/recipe"]);
        
      }
      )  
    }
}
