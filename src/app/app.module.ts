import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RegisterComponent } from './Components/register/register.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReceipesComponent } from './Components/receipes/receipes.component';
import { CreateRecipeComponent } from './Components/create-recipe/create-recipe.component';
import { EditComponentComponent } from './Components/edit-component/edit-component.component';


@NgModule({
  declarations: [
    AppComponent,
    
    RegisterComponent,
    ReceipesComponent,
    CreateRecipeComponent,
    EditComponentComponent,
    
         
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
